#include <bits/stdc++.h>
using namespace std;
int main()
{
    int n, q;
    cin >> n >> q;
    int a[n];   //O(n) emit it after calculation
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
///Ascending sorting 
// O(nlogn) = 10^5 range complete by 1s
sort(a,a+n);  // O(nlogn)
    while (q--) //O(q)  //O(q)+O(logn) = O(nlogn) //both are same so ans it
    {
        int x;
        cin >> x;
        bool flag = false;
        int l=0, r=n-1;
        //binary search 
        //O(nlogn) time complexity
        while (l<=r) //O(logn)
        {
            int mid_index=(l+r)/2;
            if(a[mid_index]==x)
            {
                flag=true;
                break;
            }
            if(x>a[mid_index])
            {
                //go right
                l=mid_index+1;
            }
            else{
                //go left
                r=mid_index-1;
            }
        }
        
        // for (int i = 0; i < n; i++)
        // {
        //     if (a[i] == x)
        //     {
        //         flag = true;
        //         break;
        //     }
        // }
        if(flag==true)cout<<"found"<<endl;
        else cout<<"not found"<< endl;
    }
    return 0;
}