#include<bits/stdc++.h>
using namespace std;

/*
1. Idea of prefix sum array : reduce time limit of code
time complexity is O(nq) 10^5*10^5=10^10 not accepted
we have to 1.5*10^5 ignore the TLE
Solution: Optimized the range sum (Cummolative Frequency)
*/

int main(){
    long long int n, q;
    cin>>n>>q;
    long long int a[n];
    for(int i=0; i<n; i++){
        cin>>a[i];
    }
    long long int pre[n];
    pre[0]=a[0];
    for(int i=1; i<n; i++)
    {
        pre[i]=a[i]+pre[i-1];   //formula of cummulative frequency
    }
// testing purpose where is my prefix okay or not
    // for(int i=0;i<n;i++)
    // {
    //     cout<<pre[i]<<" ";
    // }

    while (q--)
    {
        long long int l, r;
        cin>>l>>r;
        l--;
        r--;
        long long int sum=0;
        if(l==0) sum=pre[r];
        else sum=pre[r]-pre[l-1];
        cout<<sum<<endl;
    }
    return 0;
}