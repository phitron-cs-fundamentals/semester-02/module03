#include<bits/stdc++.h>
using namespace std;

/*
1. Range sum query means Brute Force Approch
2. Idea of prefix sum array : reduce time limit of code
time complexity is O(nq) 10^5*10^5=10^10 not accepted
we have to 1.5*10^5 ignore the TLE
Solution: range sum (Cummolative Frequency)
*/

int main(){
    int n, q;
    cin>>n>>q;
    int a[n];
    for(int i=0; i<n; i++){
        cin>>a[i];
    }
    while (q--)
    {
        int l, r;
        cin>>l>>r;
        l--;
        r--;
        int sum=0;
        for(int i=l;i<=r;i++)
        {
            sum += a[i];
        }
        cout<<sum<<endl;
    }
    return 0;
}